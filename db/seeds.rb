User.create!(
  name: "管理人",
  email: "admin@example.com",
  password: "password",
  password_confirmation: "password",
  introduction: "はじめまして。新卒から3年間WEBディレクター、1年弱プランナーを経験し、現在はWEBエンジニアを目指しています。\r\n\r\nディレクターとしてスキルアップするために始めたプログラミング学習ですが、自分でモノ作りできること面白さに魅力を感じ、プログラミングに夢中になっていきました。\r\nそして、エンジニアの仕事にチャレンジしたい気持ちが強くなる一方でしたので、転職することを決めました。\r\n\r\nプログラミングスクールで4ヶ月間Ruby,Ruby on Railsを学び、現在は独学で平日12時間、休日6時間を目安に、勉強に励んでいます。",
  skill_icon_1: "far fa-gem",
  skill_title_1: "プログラミング",
  skill_description_1: "WEBエンジニアへの転職を目指して、プログラミングを学んでいます。\r\nWEBディレクター・プランナーの経験から、エンジニアとして技術を高めるのはもちろんですが、ユーザー起点で考えられるサービス志向のエンジニアを目指します。\r\n学習中：Ruby, Ruby on Rails, JavaScript",
  skill_icon_2: "far fa-compass",
  skill_title_2: "ディレクション",
  skill_description_2: "WEBディレクターとして、主にクライアントのWEBサイト制作のディレクションを担当しました。\r\n業務内容は主に顧客折衝や企画、サイト設計、デザイン指示、外部パートナーの管理、スケジュール・クオリティ・予算の管理です。\r\nディレクションだけでなく、クラウドサービスを使用したフォーム構築、システム構築も担当しておりました。",
  skill_icon_3: "far fa-lightbulb",
  skill_title_3: "プランニング",
  skill_description_3: "プランナーとして、主にクライアントの商品プロモーションの企画提案を担当しました。\r\n実際にクライアントの商品を使う、店舗に訪れる、SNSで口コミをリサーチするなどの行い、ユーザー目線で企画を考えるようにしていました。",
  admin: true
)

user = User.last

Work.create!(
  name: "Instagram風SNS",
  description: "Rails Tutorialで学んだことを活かして、Instagram風SNSを開発しました。\r\n特にRspecでのテストに力を入れました",
  repository: "https://bitbucket.org/shogo-h/insta_clone/src/master/",
  production: "https://gentle-fjord-33503.herokuapp.com/",
  user_id: user.id,
  summary: "Rails Tutorialで学んだことを活かして、Instagram風SNSを開発しました。"
)

Work.create!(
  name: "ハンバーガー専用口コミサイト",
  description: "個人で0からWEBサービスを開発しました。\r\n自分の好きなハンバーガー屋の口コミを投稿するサービスです。\r\nECサイト、Instagram風SNSの開発で学んだことを活かして開発しました。",
  repository: "https://bitbucket.org/shogo-h/burger_sns/src/master/",
  production: "https://burger-mania-2019.herokuapp.com/",
  user_id: user.id,
  summary: "自分の好きなハンバーガー屋の口コミを投稿するサービスです。"
)

Work.create!(
  name: "ECサイト開発",
  description: "プログラミングスクール「ポテパンキャンプ」にて、3ヶ月間に渡り現役Rubyエンジニアからコードレビューを受けて、開発を行いました。\r\n主に行なったことは、環境構築、Solidusの解析、DBにある商品情報の表示（商品詳細、カテゴリー、関連商品の表示など）、テストコード（Rspec）です。",
  repository: "https://bitbucket.org/shogo-h/potepanec/src/master/",
  production: "https://thawing-dawn-98004.herokuapp.com/potepan/products/3",
  user_id: user.id,
  summary: "Solidusを使用してECサイトを作りました。"
)

Work.create!(
  name: "ポートフォリオサイト",
  description: "エンジニアとしてのポートフォリオサイトを作りました。\r\nDBにユーザー情報と作品情報を持たせて、管理画面を作成することで、簡単にサイトを更新できるようにしました。",
  repository: "https://bitbucket.org/shogo-h/portfolio-site/src/master/",
  production: "https://sh-myportfolio.herokuapp.com/",
  user_id: user.id,
  summary: "エンジニアとしてのポートフォリオサイトを作りました。"
)
