class CreateWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :works do |t|
      t.string :name, null: false
      t.text :description
      t.string :repository
      t.string :production

      t.timestamps
    end
  end
end
