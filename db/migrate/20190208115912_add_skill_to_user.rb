class AddSkillToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :skill_title_1, :string
    add_column :users, :skill_description_1, :string
    add_column :users, :skill_title_2, :string
    add_column :users, :skill_description_2, :string
    add_column :users, :skill_title_3, :string
    add_column :users, :skill_description_3, :string
  end
end
