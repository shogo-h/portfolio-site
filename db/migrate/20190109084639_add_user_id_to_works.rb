class AddUserIdToWorks < ActiveRecord::Migration[5.2]
  def up
    execute 'DELETE FROM works;'
    add_reference :works, :user, null: false, index: true
  end

  def down
    remove_reference :works, :user, index: true
  end
end
