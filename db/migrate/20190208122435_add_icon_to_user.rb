class AddIconToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :skill_icon_1, :string
    add_column :users, :skill_icon_2, :string
    add_column :users, :skill_icon_3, :string
  end
end
