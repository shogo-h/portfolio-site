Rails.application.routes.draw do
  root to: 'static_pages/index', to: 'static_pages#index'
  get '/works', to: 'static_pages#works'
  get '/works/:id', to: 'static_pages#work', as: :work
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  namespace :admin do
    resources :users, only: [:edit, :show, :index, :update]
    resources :works
  end
end
