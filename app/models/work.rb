class Work < ApplicationRecord
  validates :name, presence: true

  belongs_to :user
  mount_uploader :image, ImageUploader
  scope :recent, -> { order(created_at: :desc) }
end
