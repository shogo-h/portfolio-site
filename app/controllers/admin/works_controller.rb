class Admin::WorksController < ApplicationController
  before_action :set_work, only: [:show, :edit, :update, :destroy]

  def new
    @work = Work.new
  end

  def index
    @works = current_user.works.recent
  end

  def show
  end

  def edit
  end

  def create
    @work = current_user.works.new(work_params)
    if @work.save
      redirect_to admin_work_path(@work), notice: "「#{@work.name}」を登録しました。"
    else
      render :new
    end
  end

  def update
    if @work.update(work_params)
      redirect_to admin_work_path(@work), notice: "「#{@work.name}」を更新しました。"
    else
      render :edit
    end
  end

  def destroy
    @work.destroy
    redirect_to admin_works_path, notice: "「#{@work.name}」を削除しました。"
  end

  private
    def work_params
      params.require(:work).permit(:name, :summary, :description, :repository, :production, :image)
    end

    def set_work
      @work = current_user.works.find(params[:id])
    end
end
