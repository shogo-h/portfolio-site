class Admin::UsersController < ApplicationController

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      redirect_to admin_user_path(@user), notice: "ユーザー「#{@user.name}」を更新しました。"
    else
      render :edit
    end
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :admin, :password, :password_confirmation, :introduction, :skill_title_1, :skill_title_2, :skill_title_3, :skill_description_1, :skill_description_2, :skill_description_3, :skill_icon_1, :skill_icon_2, :skill_icon_3)
    end

    def require_admin
      redirect_to root_path unless current_user.admin?
    end
end
