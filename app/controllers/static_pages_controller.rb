class StaticPagesController < ApplicationController
  layout "static_pages"
  skip_before_action :login_required

  def index
    @user = User.find_by(admin: true)
    @works = Work.all.order(created_at: :desc).limit(4)
    @works_count = Work.all.size
  end

  def works
    @works = Work.all.order(created_at: :desc)
  end

  def work
    @work = Work.find(params[:id])
  end
end
