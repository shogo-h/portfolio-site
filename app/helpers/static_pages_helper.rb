module StaticPagesHelper
  def display_work_image(work)
    if work.image?
      image_tag(work.image.url, class: "work-image", alt: "制作事例:#{work.name}")
    else
      image_tag("/assets/tree.jpg", class: "work-image", alt: "制作事例:#{work.name}")
    end
  end
end
